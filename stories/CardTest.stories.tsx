import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import CardTest, { CardProps } from '../components/Card';

export default {
  title: 'Example/Card',
  component: CardTest,
} as Meta;

const Template: Story<CardProps> = (args) => <CardTest {...args} />;

export const Normal = Template.bind({});
Normal.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/100',
  text: 'Body',
  link: '183 Discussions'
};

export const LongBodyText = Template.bind({});
LongBodyText.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/100',
  text: 'Dummies Text: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, ',
  link: '183 Discussions'
};  

export const EmptyBodyText = Template.bind({});
EmptyBodyText.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/100',
  text: '',
  link: '183 Discussions'
};

export const LargeImage = Template.bind({});
LargeImage.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/1000x1000',
  text: 'Body',
  link: '183 Discussions'
};

export const SmallImage = Template.bind({});
SmallImage.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/10x10',
  text: 'Body',
  link: '183 Discussions'
};

export const WideImage = Template.bind({});
WideImage.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/1000x100',
  text: 'Body',
  link: '183 Discussions'
};

export const TallImage = Template.bind({});
TallImage.args = {
  label: 'Header',
  image: 'https://via.placeholder.com/100x1000',
  text: 'Body',
  link: '183 Discussions'
};