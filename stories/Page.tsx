/** @jsxImportSource @emotion/core */

import { jsx, css } from '@emotion/react'
import React from 'react';
import TabTest from '../components/Tab';
import CardTest from '../components/Card';
import { useState, useEffect } from 'react';
import './page.css'

export interface PageProps {
  
}

export const Page: React.FC<PageProps> = () => {

  const [tabKey, setTabKey] = useState(1)
  
  const setTab = (id :number) => {
    setTabKey(id)
  }

  return (
    <>
      <div css={css`width: 345px; background-color: rgb(243,239,234); padding: 15px;`}>
        <TabTest
          tab1={'Prevention'}
          tab2={'Symptoms'}
          tab3={'Diagnose'}
          onClick={(id) => setTab(id)}
        />
         <CardTest
            label={tabKey == 1 ? 'Ninja' : ( tabKey == 2 ? 'Ninja 2' : 'Ninja 3')}
            image={'https://i.pinimg.com/236x/9d/c5/0d/9dc50dbd85f102f0b4887c18d3e867cd.jpg'}
            text={tabKey == 1 ? 'Wear a mask' : ( tabKey == 2 ? 'Wear a mask 2' : 'Wear a mask 3')}
            link={tabKey == 1 ? '11 Discussions' : ( tabKey == 2 ? '22 Discussions' : '33 Discussions')}
          />
          <CardTest
            label={tabKey == 1 ? 'Ninja 4' : ( tabKey == 2 ? 'Ninja 5' : 'Ninja 6')}
            image={'https://i.pinimg.com/236x/9d/c5/0d/9dc50dbd85f102f0b4887c18d3e867cd.jpg'}
            text={tabKey == 1 ? 'Wear a mask 4' : ( tabKey == 2 ? 'Wear a mask 5' : 'Wear a mask 6')}
            link={tabKey == 1 ? '44 Discussions' : ( tabKey == 2 ? '55 Discussions' : '66 Discussions')}
          />
      </div>
    </>
  );
}
