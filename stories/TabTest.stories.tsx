import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import TabTest, { TabProps } from '../components/Tab';

export default {
  title: 'Example/Tab',
  component: TabTest,
} as Meta;

const Template: Story<TabProps> = (args) => <TabTest {...args} />;

export const Normal = Template.bind({});
Normal.args = {
  tab1: 'Prevention',
  tab2: 'Symptoms',
  tab3: 'Diagnose'
};


