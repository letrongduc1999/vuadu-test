/** @jsxImportSource @emotion/core */

import { jsx, css } from '@emotion/react'
import React from 'react';
import { useState } from 'react';
import './index.css'

import {
  Flex,
  Button
} from 'rebass'

export interface TabProps {
  tab1: string;
  tab2: string;
  tab3: string;
  onClick?: (index) => void;
}

const base = {
  color: 'rgb(194,194,181)',
  backgroundColor: 'white',
  borderRadius: '20px',
  cursor: 'pointer',
  fontSize: '15px',
  outline: 'none !important',
  width: '33.33333333%',
  padding: '10px 0px',
  fontWeight: 'bold',
  fontFamily: `'Airbnb Cereal App', sans-serif`
}
const active = {
  color: '#7DA752 !important',
  backgroundColor: 'rgba(125,167,82,0.2)',
  borderRadius: '20px',
  cursor: 'pointer',
  fontSize: '15px',
  outline: 'none !important',
  width: '33.33333333%',
  fontWeight: 'bold',
  padding: '10px 0px',
  fontFamily: `'Airbnb Cereal App', sans-serif`
}
/**
 * Primary UI component for user interaction
 */
export const TabTest: React.FC<TabProps> = ({
  tab1,
  tab2,
  tab3,
  onClick,
  ...props
}) => {

  const setTab = (id) => {
    setBtnKey(id)
    onClick(id)
  }

  const [btnKey, setBtnKey] = useState(1);
  return (
    <Flex sx={{
      borderRadius: '30px',
      backgroundColor: 'white', 
      width: '345px',
      padding: '5px 5px',
      marginBottom: '20px',
      marginRight: '500px'
    }}>
      <Button sx={btnKey == 1 ?  active : base} onClick={() => setTab(1)}>{tab1}</Button>
      <Button sx={btnKey == 2 ?  active : base} onClick={() => setTab(2)}>{tab2}</Button>
      <Button sx={btnKey == 3 ?  active : base} onClick={() => setTab(3)}>{tab3}</Button>
    </Flex>
  );
};

export default TabTest
