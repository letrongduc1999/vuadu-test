/** @jsxImportSource @emotion/core */

import { css, jsx } from '@emotion/react'
import React from 'react';
import './index.css'

import {
  Card,
  Image,
  Heading,
} from 'rebass'

export interface CardProps {
  label: string;
  image: string;
  text: string;
  link: string;
  onClick?: () => void;
}


/**
 * Primary UI component for user interaction
 */
const CardTest: React.FC<CardProps> = ({
  label,
  image,
  text,
  link,
  ...props
}) => {
  return (
    <>
    <Card sx={cardCSS}>
      <Image src={image} sx={imageCSS}/>
      <div css={css`width: 66.666666%; margin-right: 10px;`}>
        <Heading sx={{color: 'black',height: '20px', lineHeight: '20px' ,fontSize: '16px', fontWeight: 'bolder !important' , fontFamily: `'Airbnb Cereal App', sans-serif`}}>
          {label}
        </Heading>
        <div css={wrapperDiv}>
          <div css={wrapperText}>
            {text}
          </div>
        </div>
        <a href="" css={linkCSS}>
          {link} →
        </a>
      </div>
    </Card>
    </>
  );
};

export default CardTest;

const cardCSS = { 
  display: 'flex',
  flexDirection: 'row',
  padding: '10px',
  backgroundColor: 'white',
  borderRadius: '16px',
  justifyContent: 'space-between',
  width: '345px',
  alignItems: 'center',
  marginBottom: '15px',
}

const imageCSS = {
  width: '30%',
  borderRadius: '10px',
  maxWidth: '100%', 
  minWidth: '100px',
  boxSizing: 'border-box',
  height: '100px',
  objectFit: 'cover',
  marginRight: '15px'
}

const wrapperDiv = css`
  box-sizing: border-box;
  margin: 0;
  min-width: 0;
  -webkit-align-items: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  width: 100%;
  height: 50px;
  padding-top: 8px;
  padding-bottom: 8px;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
`
const wrapperText = css`
  box-sizing: border-box;
  margin: 0;
  min-width: 0;
  font-family: 'Airbnb Cereal App', sans-serif;
  font-size: 13px;
  color: rgb(121,130,130);
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`

const linkCSS = css`
  box-sizing: border-box;
  margin: 0;
  min-width: 0;
  -webkit-text-decoration: none;
  text-decoration: none;
  font-weight: 600;
  font-family: 'Airbnb Cereal App', sans-serif;
  font-size: 15px;
  cursor: pointer;
  color: #7da752;
`