import Head from 'next/head'
import styles from '../styles/Home.module.css'
import CardTest from '../components/Card'
import TabTest from '../components/Tab'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <CardTest 
          label={'Wear a mask'}
          image={'https://i.pinimg.com/236x/9d/c5/0d/9dc50dbd85f102f0b4887c18d3e867cd.jpg'}
          text={'You should wear a facemask when you are around people'}
          link={'183 Discussions'}
        />
        <hr/>
        <TabTest
          tab1={'Prevention'}
          tab2={'Symptoms'}
          tab3={'Diagnose'}
        />
      </main>
    </div>
  )
}
